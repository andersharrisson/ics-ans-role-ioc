# ics-ans-role-ioc

Ansible role to install an IOC.

The `iocs` variable is a list of iocs to instantiate on the system. It should conform to the following structure:
```
iocs:
  - name: <name of the IOC>
    git: <git repository of IOC configuration, default: {{ ioc_git_url }}/{{ ioc_git_namespace }}/{{ name }} >
    version: <git repository version, e.g. tag, branch or commit id, default: HEAD >
```

## Role Variables

See [default vars](defaults/main.yml).

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-ioc
```

## License

BSD 2-clause
