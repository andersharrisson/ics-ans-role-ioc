import re

CAGET = "/tmp/caget"


def caget(host, pv):
    return host.run(f"{CAGET} -w 5 -t {pv}")


def sluggify(ioc):
    return re.sub("[^a-zA-Z0-9_-]", "_", ioc)
