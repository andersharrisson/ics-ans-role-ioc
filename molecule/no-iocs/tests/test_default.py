import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("ioc_hosts")


def test_iocuser_is_present(host):
    user = host.user("iocuser")
    assert user.uid == 1042


def test_nfs_mounts_are_mounted(host):
    assert host.mount_point("/epics")
