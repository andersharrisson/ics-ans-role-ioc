---
- name: Create IOC slugs
  set_fact:
    new_ioc: "{{ item | combine({'slug': item.name | regex_replace(ioc_name_pattern, ioc_name_replace)}) }}"
  register: new_iocs
  loop: "{{ iocs }}"

- name: Add IOC slugs to IOC data
  set_fact:
    iocs: "{{ new_iocs.results | map(attribute='ansible_facts.new_ioc') | list }}"

- name: Set environment variable fact
  set_fact:
    ioc_env:
      AS_TOP: "{{ ioc_nonvolatile_path }}"
      LOG_SERVER_NAME: "{{ ioc_log_server_name }}"
      ERRORLOG_SERVER_PORT: "{{ ioc_errorlog_server_port | default(omit) }}"
      CAPUTLOG_SERVER_PORT: "{{ ioc_caputlog_server_port | default(omit) }}"
      ASG_FILENAME: "{{ ioc_asg_filename | default(omit) }}"
      FACNAME: "{{ ioc_facility_name | default(omit) }}"

- name: Clone IOC repository
  git:
    repo: "{{ ioc.git }}"
    dest: "{{ ioc_iocs_folder }}/{{ ioc.slug }}"
    version: "{{ ioc.version }}"
    update: true
  become: true
  become_user: "{{ ioc_user_name }}"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  register: ioc_source
  notify:
    - restart IOCs

- name: Read in json metadata
  slurp:
    src: "{{ ioc_iocs_folder }}/{{ ioc.slug }}/ioc.json"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  register: _slurp_json
  ignore_errors: true

- name: Parse json data
  set_fact:
    json_metadata: "{{ range(_slurp_json.results | length) |
      map('extract', _slurp_json.results, morekeys='content') |
      map('default', ioc_empty_metadata_slurp) |
      map('b64decode') |
      map('from_json') | list }}"

- name: Combine default, json, and IOC data
  set_fact:
    ioc_data: "{{ ioc_default_metadata | combine(item[0]) | combine(item[1]) }}"
  loop: "{{ json_metadata | zip(iocs) | list }}"
  register: combined_metadata

- name: Combine all IOC data
  set_fact:
    iocs: "{{ combined_metadata.results | map(attribute='ansible_facts.ioc_data') | list }}"

- name: Create IOC runtime directory
  file:
    path: "{{ ioc_runtime_folder }}/{{ ioc.slug }}"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 02755
    state: directory
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Create IOC directory on nonvolatile nfs server
  file:
    path: "{{ ioc_nonvolatile_server_path }}/{{ ioc.slug }}"
    state: directory
    owner: "{{ ioc_user_id }}"
    group: root
    mode: 0755
  delegate_to: "{{ ioc_nonvolatile_server }}"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Mount nonvolatile share
  mount:
    src: "{{ ioc_nonvolatile_server }}:{{ ioc_nonvolatile_server_path }}/{{ ioc.slug }}"
    path: "{{ ioc_nonvolatile_path }}/{{ ioc.slug }}"
    fstype: "{{ ioc_nonvolatile_mount_fstype }}"
    opts: "{{ ioc_nonvolatile_mount_opts }}"
    state: mounted
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Create Conda environment
  conda_env:
    name: "{{ ioc_conda_envs_directory }}/{{ ioc.slug }}"
    state: present
    file: "{{ ioc_iocs_folder }}/{{ ioc.slug }}/environment.yaml"
  when: ioc.ioc_type == "conda"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  register: ioc_environment
  notify:
    - restart IOCs
  become: true
  become_user: "{{ ioc_user_name }}"

- name: Create Conda environment activation directory
  file:
    path: "{{ ioc_conda_envs_directory }}/{{ ioc.slug }}/etc/conda/activate.d"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 02755
    state: directory
  when: ioc.ioc_type == "conda"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Create Conda environment deactivation directory
  file:
    path: "{{ ioc_conda_envs_directory }}/{{ ioc.slug }}/etc/conda/deactivate.d"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 02755
    state: directory
  when: ioc.ioc_type == "conda"
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Mount NFS e3 share
  mount:
    path: "{{ _share.path }}"
    src: "{{ _share.src }}"
    fstype: "{{ _share.fstype | default('nfs') }}"
    opts: "{{ _share.opts | default('ro') }}"
    state: mounted
  when: iocs | selectattr('ioc_type', 'equalto', 'nfs') | list | length > 0
  loop: "{{ ioc_nfs_mountpoints }}"
  loop_control:
    loop_var: _share

- name: Add prometheus exporter configuration file
  copy:
    content: "{{ ioc | to_json(sort_keys=True) }}"
    dest: "{{ ioc_runtime_folder }}/{{ ioc.slug }}/metadata.json"
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0644
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  register: metadata_json
  notify:
    - restart prometheus exporters
    - restart IOCs

- name: Create service to autostart IOC
  template:
    src: "ioc.service.j2"
    dest: "/etc/systemd/system/ioc-{{ ioc.slug }}.service"
    owner: root
    group: root
    mode: 0644
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  register: ioc_variables
  notify:
    - reload systemd config
    - restart IOCs

############################ We need to clean up an old IOC if it exists

- name: Disable old services
  service:
    name: "ioc@{{ ioc.slug }}"
    state: stopped
    enabled: false
  register: service_stop
  failed_when:
    - service_stop.failed
    - '"Could not find the requested service" not in service_stop.msg'
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Remove old systemd override dir
  file:
    path: "/etc/systemd/system/ioc@{{ ioc.slug }}.service.d"
    state: absent
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

############################ End cleanup block

- name: Ensure IOC service is running and enabled
  service:
    name: "ioc-{{ ioc.slug }}"
    state: started
    enabled: true
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc

- name: Copy IOC console configuration
  template:
    src: console.cf.j2
    dest: /etc/conserver/{{ ioc.slug }}.cf
    owner: "{{ ioc_user_name }}"
    group: "{{ ioc_group_name }}"
    mode: 0644
  loop: "{{ iocs }}"
  loop_control:
    loop_var: ioc
  notify:
    - reload conserver
